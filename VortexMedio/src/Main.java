import java.util.Random;
import java.util.Scanner;

public class Main {

	public static void exibirConstelacao(int[][] constelacao) {
		System.out.println("[");
		for (int i = 0; i < constelacao.length; i++) {
			System.out.print("[");
			for (int j = 0; j < constelacao.length; j++) {
				if (j + 1 != constelacao.length) {
					System.out.print(constelacao[i][j] + ", ");
				} else {
					System.out.print(constelacao[i][j]);
				}
			}
			if (i + 1 != constelacao.length) {
				System.out.println("],");
			} else {
				System.out.println("]");
			}
		}
		System.out.println("]");
	}

	public static void main(String[] args) {
		Scanner entrada = new Scanner(System.in);
		int estrelas = 2;
		while (estrelas < 4 || estrelas > 8) {
			System.out.println("Quantas estrelas ser�o?(Min. 4 e Max 8)");
			estrelas = entrada.nextInt();
			if (estrelas < 4 || estrelas > 8) {
				System.out.println("Numero inaceitavel de estrelas");
			}
		}
		Random random = new Random();
		int[][] constelacao = new int[estrelas][estrelas];
		int aux = 0;
		for (int i = 0; i < estrelas; i++) {
			for (int j = 0 + aux; j < estrelas; j++) {
				if (i == j) {
					constelacao[i][j] = 0;
				} else {
					int num = random.nextInt(2);
					constelacao[i][j] = num;
					constelacao[j][i] = num;
				}
			}
			aux++;
		}
		// Exibir constelacao
		exibirConstelacao(constelacao);
		int estrela1 = 20;
		do {
			System.out.println(
					"Quais estrelas voce quer saber se estao interligadas?(separadas por espa�o) digite 0 para sair");
			estrela1 = entrada.nextInt();
			if (estrela1 != 0) {
				int estrela2 = entrada.nextInt();
				if (estrela1 > estrelas || estrela2 > estrelas) {
					System.out.println("Estrelas invalidas");
				} else {
					if (constelacao[estrela1 - 1][estrela2 - 1] == 1) {
						System.out.println("Ha ligacao");
					} else {
						System.out.println("Nao ha ligacao");
					}
				}
			}
		} while (estrela1 != 0);

	}
}

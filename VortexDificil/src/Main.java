import java.util.Random;

public class Main {

	public static int pares(Cartas[] carta) {
		int par = 0, numCarta = 0, pontos = 0, qntdPares = 0;
		boolean trinca = false, temPar = false;
		for (int i = 0; i < carta.length - 1; i++) {
			if (carta[i].num != numCarta) {
				for (int j = 1 + i; j < carta.length; j++) {
					if (carta[i].num == carta[j].num) {
						par++;
						numCarta = carta[i].num;
					}
				}
				if (par > 0) {
					switch (par) {
					case 1:
						qntdPares++;
						if (qntdPares > 1) {
							if (pontos <= 2) {
								pontos = 2;
							}
						} else {
							if (trinca) {
								if (pontos <= 6) {
									pontos = 6;
								}
							}
							temPar = true;
							if (pontos <= 1) {
								pontos = 1;
							}
						}
						break;
					case 2:
						trinca = true;
						if (temPar) {
							if (pontos <= 6) {
								pontos = 6;
							}
						}
						if (pontos <= 3) {
							pontos = 3;
						}
						break;
					case 3:
						if (pontos <= 7) {
							pontos = 7;
						}
						break;
					}
				}
				par = 0;
			}
		}
		return pontos;
	}

	public static int Sequencia(Cartas[] carta) {
		int contadorS = 1, ponto = 0;
		for (int i = 0; i < carta.length; i++) {
			if (i + 1 <= carta.length - 1) {
				if (carta[i].num != carta[i + 1].num) {
					if (carta[i + 1].num == carta[i].num + 1) {
						contadorS++;
						if (contadorS == 5) {
							ponto = 4;
							break;
						} else if (carta[i + 1].num == 13 && carta[0].num == 1) {
							contadorS++;
							if (contadorS == 5) {
								ponto = 4;
								break;
							}
						}
					} else {
						contadorS = 1;
					}
				}
			}
		}
		return ponto;
	}

	public static int Flush(Cartas[] robo) {
		int contadorE = 0, contadorO = 0, contadorP = 0, contadorC = 0, ponto = 0;
		for (int i = 0; i < robo.length; i++) {
			switch (robo[i].naipe) {
			case "E":
				contadorE++;
				break;
			case "O":
				contadorO++;
				break;
			case "P":
				contadorP++;
				break;
			case "C":
				contadorC++;
				break;
			}
		}
		if (contadorE >= 5 || contadorO >= 5 || contadorP >= 5 || contadorC >= 5) {
			ponto = 5;
		}
		return ponto;
	}

	public static int RoyalFlush(Cartas[] robo) {
		int contadorE = 0, contadorC = 0, contadorO = 0, contadorP = 0, ponto = 0;
		for (int i = 0; i < robo.length; i++) {
			switch (robo[i].num) {
			case 1:
				switch (robo[i].naipe) {
				case "E":
					contadorE++;
					break;
				case "C":
					contadorC++;
					break;
				case "O":
					contadorO++;
					break;
				case "P":
					contadorP++;
					break;
				}
				break;
			case 10:
				switch (robo[i].naipe) {
				case "E":
					contadorE++;
					break;
				case "C":
					contadorC++;
					break;
				case "O":
					contadorO++;
					break;
				case "P":
					contadorP++;
					break;
				}
				break;
			case 11:
				switch (robo[i].naipe) {
				case "E":
					contadorE++;
					break;
				case "C":
					contadorC++;
					break;
				case "O":
					contadorO++;
					break;
				case "P":
					contadorP++;
					break;
				}
				break;
			case 12:
				switch (robo[i].naipe) {
				case "E":
					contadorE++;
					break;
				case "C":
					contadorC++;
					break;
				case "O":
					contadorO++;
					break;
				case "P":
					contadorP++;
					break;
				}
				break;
			case 13:
				switch (robo[i].naipe) {
				case "E":
					contadorE++;
					break;
				case "C":
					contadorC++;
					break;
				case "O":
					contadorO++;
					break;
				case "P":
					contadorP++;
					break;
				}
				break;
			}
		}
		if (contadorE == 5 || contadorO == 5 || contadorC == 5 || contadorP == 5) {
			ponto = 9;
		}
		return ponto;
	}

	public static int StraightFlush(Cartas[] robo) {
		int contador1 = 0, contador2 = 0, contador3 = 0, aux2 = 0, ponto = 0;
		String aux = null;
		Cartas[] vetorTeste1 = new Cartas[5];
		Cartas[] vetorTeste2 = new Cartas[5];
		Cartas[] vetorTeste3 = new Cartas[5];
		for (int i = 0; i < robo.length; i++) {
			if (vetorTeste1[0] == null) {
				vetorTeste1[contador1] = robo[i];
				contador1++;
			} else if (vetorTeste1[0] != null && robo[i].naipe == vetorTeste1[contador1 - 1].naipe) {
				if (robo[i].num == vetorTeste1[contador1 - 1].num + 1) {
					if (vetorTeste1[4] != null) {
						for (int j = 0; j < vetorTeste1.length - 1; j++) {
							vetorTeste1[j] = vetorTeste1[j + 1];
						}
						vetorTeste1[vetorTeste1.length - 1] = robo[i];
					} else {
						vetorTeste1[contador1] = robo[i];
						contador1++;
					}
				} else {
					aux = robo[i].naipe;
					robo[i].naipe = "X";
					i--;
				}
			} else if (aux != null && aux2 == 0) {
				robo[i].naipe = aux;
				aux = null;
				aux2++;
				if (vetorTeste2[0] == null) {
					vetorTeste2[contador2] = robo[i];
					contador2++;
					aux2 = 0;
				} else if (vetorTeste2[0] != null && robo[i].naipe == vetorTeste2[contador2 - 1].naipe) {
					if (vetorTeste2 == null || robo[i].num == vetorTeste2[contador2 - 1].num + 1) {
						if (vetorTeste2[4] != null) {
							for (int j = 0; j < vetorTeste2.length - 1; j++) {
								vetorTeste2[j] = vetorTeste2[j + 1];
							}
							vetorTeste2[vetorTeste2.length - 1] = robo[i];
							aux2 = 0;
						} else {
							vetorTeste2[contador2] = robo[i];
							contador2++;
							aux2 = 0;
						}
					} else {
						aux = robo[i].naipe;
						robo[i].naipe = "X";
						// i--;
					}
				} else {
					if (vetorTeste3[0] == null) {
						vetorTeste3[contador3] = robo[i];
						contador3++;
						aux2 = 0;
					} else if (vetorTeste3[0] != null && robo[i].naipe == vetorTeste3[contador3 - 1].naipe) {
						if (vetorTeste3 == null || robo[i].num == vetorTeste3[contador3 - 1].num + 1) {
							if (vetorTeste3[4] != null) {
								for (int j = 0; j < vetorTeste3.length - 1; j++) {
									vetorTeste3[j] = vetorTeste3[j + 1];
								}
								vetorTeste3[vetorTeste3.length - 1] = robo[i];
								aux2 = 0;
							} else {
								vetorTeste3[contador3] = robo[i];
								contador3++;
								aux2 = 0;
							}
						} else {
							break;
						}
					} else {
						break;
					}
				}
			} else {
				if (vetorTeste2[0] == null) {
					vetorTeste2[contador2] = robo[i];
					contador2++;
					aux2 = 0;
				} else if (vetorTeste2[0] != null && robo[i].naipe == vetorTeste2[contador2 - 1].naipe) {
					if (vetorTeste2 == null || robo[i].num == vetorTeste2[contador2 - 1].num + 1) {
						if (vetorTeste2[4] != null) {
							for (int j = 0; j < vetorTeste2.length - 1; j++) {
								vetorTeste2[j] = vetorTeste2[j + 1];
							}
							vetorTeste2[vetorTeste2.length - 1] = robo[i];
							aux2 = 0;
						} else {
							vetorTeste2[contador2] = robo[i];
							contador2++;
							aux2 = 0;
						}
					} else {
						aux = robo[i].naipe;
						robo[i].naipe = "X";
						i--;
					}
				} else {
					if (vetorTeste3[0] == null) {
						vetorTeste3[contador3] = robo[i];
						contador3++;
						aux2 = 0;
					} else if (vetorTeste3[0] != null && robo[i].naipe == vetorTeste3[contador3 - 1].naipe) {
						if (vetorTeste3 == null || robo[i].num == vetorTeste3[contador3 - 1].num + 1) {
							if (vetorTeste3[4] != null) {
								for (int j = 0; j < vetorTeste3.length - 1; j++) {
									vetorTeste3[j] = vetorTeste3[j + 1];
								}
								vetorTeste3[vetorTeste3.length - 1] = robo[i];
								aux2 = 0;
							} else {
								vetorTeste3[contador3] = robo[i];
								contador3++;
								aux2 = 0;
							}
						} else {
							break;
						}
					} else {
						break;
					}
				}
			}
			if (aux != null && aux2 > 0) {
				robo[i].naipe = aux;
				aux = null;
				if (vetorTeste3[0] == null) {
					vetorTeste3[contador3] = robo[i];
					contador3++;
					aux2 = 0;
				} else if (vetorTeste3[0] != null && robo[i].naipe == vetorTeste3[contador3 - 1].naipe) {
					if (vetorTeste3 == null || robo[i].num == vetorTeste3[contador3 - 1].num + 1) {
						if (vetorTeste3[4] != null) {
							for (int j = 0; j < vetorTeste3.length - 1; j++) {
								vetorTeste3[j] = vetorTeste3[j + 1];
							}
							vetorTeste3[vetorTeste3.length - 1] = robo[i];
							aux2 = 0;
						} else {
							vetorTeste3[contador3] = robo[i];
							contador3++;
							aux2 = 0;
						}
					} else {
						break;
					}
				} else {
					break;
				}
			}

		}
		if (vetorTeste1[4] != null || vetorTeste2[4] != null || vetorTeste3[4] != null) {
			ponto = 8;
		}
		return ponto;
	}

	public static Cartas[] Ordenacao(Cartas[] robo) {
		Cartas troca;
		for (int i = 0; i < robo.length; i++) {
			for (int j = i + 1; j < robo.length; j++) {
				if (robo[i].num > robo[j].num) {
					troca = robo[i];
					robo[i] = robo[j];
					robo[j] = troca;
				}
			}
		}
		return robo;
	}

	static class Cartas {
		public int num;
		public String naipe;

		Cartas(int num, String naipe) {
			this.num = num;
			this.naipe = naipe;
		}

		public void display() {
			System.out.print(num + naipe + " ");
		}
	}

	public static void main(String[] args) {
		Random random = new Random();
		Cartas[] baralho = new Cartas[] { new Cartas(1, "P"), new Cartas(2, "P"), new Cartas(3, "P"),
				new Cartas(4, "P"), new Cartas(5, "P"), new Cartas(6, "P"), new Cartas(7, "P"), new Cartas(8, "P"),
				new Cartas(9, "P"), new Cartas(10, "P"), new Cartas(11, "P"), new Cartas(12, "P"), new Cartas(13, "P"),
				new Cartas(1, "O"), new Cartas(2, "O"), new Cartas(3, "O"), new Cartas(4, "O"), new Cartas(5, "O"),
				new Cartas(6, "O"), new Cartas(7, "O"), new Cartas(8, "O"), new Cartas(9, "O"), new Cartas(10, "O"),
				new Cartas(11, "O"), new Cartas(12, "O"), new Cartas(13, "O"), new Cartas(1, "E"), new Cartas(2, "E"),
				new Cartas(3, "E"), new Cartas(4, "E"), new Cartas(5, "E"), new Cartas(6, "E"), new Cartas(7, "E"),
				new Cartas(8, "E"), new Cartas(9, "E"), new Cartas(10, "E"), new Cartas(11, "E"), new Cartas(12, "E"),
				new Cartas(13, "E"), new Cartas(1, "C"), new Cartas(2, "C"), new Cartas(3, "C"), new Cartas(4, "C"),
				new Cartas(5, "C"), new Cartas(6, "C"), new Cartas(7, "C"), new Cartas(8, "C"), new Cartas(9, "C"),
				new Cartas(10, "C"), new Cartas(11, "C"), new Cartas(12, "C"), new Cartas(13, "C") };
		// INICIALIZACAO
		int rndNum[] = new int[9];
		for (int i = 0; i < 9; i++) {
			int num = random.nextInt(52);
			int aux = 0;
			for (int j = 0; j < rndNum.length; j++) {
				if (num == rndNum[j]) {
					aux++;
					break;
				}
			}
			if (aux == 0) {
				rndNum[i] = num;
			} else {
				i--;
			}
		}
		Cartas[] mesa = new Cartas[5];
		Cartas[] robo1 = new Cartas[2];
		Cartas[] robo2 = new Cartas[2];
		System.out.print("Mesa: ");
		for (int i = 0; i < mesa.length; i++) {
			mesa[i] = baralho[rndNum[i]];
			if (mesa[i].num == 1 || mesa[i].num == 11 || mesa[i].num == 12 || mesa[i].num == 13) {
				switch (mesa[i].num) {
				case 1:
					System.out.print("A" + mesa[i].naipe + " ");
					break;
				case 11:
					System.out.print("J" + mesa[i].naipe + " ");
					break;
				case 12:
					System.out.print("Q" + mesa[i].naipe + " ");
					break;
				case 13:
					System.out.print("K" + mesa[i].naipe + " ");
					break;
				}
			} else {
				System.out.print(mesa[i].num + mesa[i].naipe + " ");
			}
		}
		System.out.print("\nRobo1: ");
		for (int i = 0; i < robo1.length; i++) {
			robo1[i] = baralho[rndNum[i + 5]];
			if (robo1[i].num == 1 || robo1[i].num == 11 || robo1[i].num == 12 || robo1[i].num == 13) {
				switch (robo1[i].num) {
				case 1:
					System.out.print("A" + robo1[i].naipe + " ");
					break;
				case 11:
					System.out.print("J" + robo1[i].naipe + " ");
					break;
				case 12:
					System.out.print("Q" + robo1[i].naipe + " ");
					break;
				case 13:
					System.out.print("K" + robo1[i].naipe + " ");
					break;
				}
			} else {
				System.out.print(robo1[i].num + robo1[i].naipe + " ");
			}

		}
		System.out.print("\nRobo2: ");
		for (int i = 0; i < robo2.length; i++) {
			robo2[i] = baralho[rndNum[i + 7]];
			if (robo2[i].num == 1 || robo2[i].num == 11 || robo2[i].num == 12 || robo2[i].num == 13) {
				switch (robo1[i].num) {
				case 1:
					System.out.print("A" + robo2[i].naipe + " ");
					break;
				case 11:
					System.out.print("J" + robo2[i].naipe + " ");
					break;
				case 12:
					System.out.print("Q" + robo2[i].naipe + " ");
					break;
				case 13:
					System.out.print("K" + robo2[i].naipe + " ");
					break;
				}
			} else {
				System.out.print(robo2[i].num + robo2[i].naipe + " ");
			}
		}

//		Cartas[] mesa = { new Cartas(1, "P"), new Cartas(5, "C"), new Cartas(9, "P"), new Cartas(10, "P"),
//				new Cartas(11, "P") };
//		Cartas[] robo1 = { new Cartas(12, "P"), new Cartas(13, "P") };
//		Cartas[] robo2 = { new Cartas(2, "E"), new Cartas(8, "E") };

		Cartas[] mesaRobo1 = new Cartas[7];
		Cartas[] mesaRobo2 = new Cartas[7];
		for (int i = 0; i < mesa.length; i++) {
			mesaRobo1[i] = mesa[i];
		}
		for (int i = 0; i < robo1.length; i++) {
			mesaRobo1[i + 5] = robo1[i];
		}
		for (int i = 0; i < mesa.length; i++) {
			mesaRobo2[i] = mesa[i];
		}
		for (int i = 0; i < robo2.length; i++) {
			mesaRobo2[i + 5] = robo2[i];
		}
		// FIM DA INICIALIZACAO

		// ORDENANDO MesaRobo1
		mesaRobo1 = Ordenacao(mesaRobo1);
		// ORDENANDO MesaRobo2
		mesaRobo2 = Ordenacao(mesaRobo2);
		System.out.println();
		// FIM DA ORDENACAO
		// QUANTOS PONTO O ROBO1 FEZ
		int ponto1 = 0, numPar1 = 0, numCartaAlta1 = 0;
		ponto1 = RoyalFlush(mesaRobo1);
		if (ponto1 == 9) {
			System.out.println("Robo1 tem um RoyalFlush");
		} else {
			ponto1 = StraightFlush(mesaRobo1);
			if (ponto1 == 8) {
				System.out.println("Robo1 tem StraightFlush");
			} else {
				ponto1 = pares(mesaRobo1);
				if (ponto1 == 7) {
					System.out.println("Robo1 tem Quadra");
				} else {
					ponto1 = pares(mesaRobo1);
					if (ponto1 == 6) {
						System.out.println("Robo1 tem FullHouse");
					} else {
						ponto1 = Flush(mesaRobo1);
						if (ponto1 == 5) {
							System.out.println("Robo1 tem Flush");
						} else {
							ponto1 = Sequencia(mesaRobo1);
							if (ponto1 == 4) {
								System.out.println("Robo1 tem Sequencia");
							} else {
								ponto1 = pares(mesaRobo1);
								if (ponto1 == 3) {
									System.out.println("Robo1 tem trinca");
								} else {
									ponto1 = pares(mesaRobo1);
									if (ponto1 == 2) {
										System.out.println("Robo1 tem dois pares");
									} else {
										ponto1 = pares(mesaRobo1);
										if (ponto1 == 1) {
											for (int i = 0; i < mesaRobo1.length; i++) {
												if (mesaRobo1[i].num == mesaRobo1[i + 1].num) {
													numPar1 = mesaRobo1[i].num;
													break;
												}
											}
											System.out.println("Robo1 tem um par");
										} else {
											numCartaAlta1 = mesaRobo1[mesaRobo1.length - 1].num;
											System.out.println("Robo2 tem carta alta");
										}
									}
								}
							}
						}
					}
				}
			}
		}
		// QUANTOS PONTOS O ROBO2 FEZ
		int ponto2 = 0, numPar2 = 0, numCartaAlta2 = 0;
		ponto2 = RoyalFlush(mesaRobo2);
		if (ponto2 == 9) {
			System.out.println("Robo2 tem um RoyalFlush");
		} else {
			ponto2 = StraightFlush(mesaRobo2);
			if (ponto2 == 8) {
				System.out.println("Robo2 tem StraightFlush");
			} else {
				ponto2 = pares(mesaRobo2);
				if (ponto2 == 7) {
					System.out.println("Robo2 tem Quadra");
				} else {
					ponto2 = pares(mesaRobo2);
					if (ponto2 == 6) {
						System.out.println("Robo2 tem FullHouse");
					} else {
						ponto2 = Flush(mesaRobo2);
						if (ponto2 == 5) {
							System.out.println("Robo2 tem Flush");
						} else {
							ponto2 = Sequencia(mesaRobo2);
							if (ponto2 == 4) {
								System.out.println("Robo2 tem Sequencia");
							} else {
								ponto2 = pares(mesaRobo2);
								if (ponto2 == 3) {
									System.out.println("Robo2 tem trinca");
								} else {
									ponto2 = pares(mesaRobo2);
									if (ponto2 == 2) {
										System.out.println("Robo2 tem dois pares");
									} else {
										ponto2 = pares(mesaRobo2);
										if (ponto2 == 1) {
											for (int i = 0; i < mesaRobo2.length; i++) {
												if (mesaRobo2[i].num == mesaRobo2[i + 1].num) {
													numPar2 = mesaRobo2[i].num;
													break;
												}
											}
											System.out.println("Robo2 tem um par");
										} else {
											numCartaAlta2 = mesaRobo2[mesaRobo2.length - 1].num;
											System.out.println("Robo2 tem carta alta");
										}
									}
								}
							}
						}
					}
				}
			}
		}
		if (numPar1 == 1) {
			numPar1 = 14;
		}
		if (numPar2 == 1) {
			numPar2 = 14;
		}
		if (numCartaAlta1 == 1) {
			numCartaAlta1 = 14;
		}
		if (numCartaAlta2 == 1) {
			numCartaAlta2 = 14;
		}
		// RESULTADO FINAL
		if (ponto1 > ponto2) {
			System.out.println("Robo1 venceu");
		} else if (ponto2 > ponto1) {
			System.out.println("Robo2 venceu");
		} else {
			if (ponto1 == 0) {
				if (numCartaAlta1 > numCartaAlta2) {
					System.out.println("Robo1 venceu");
				} else if (numCartaAlta2 > numCartaAlta1) {
					System.out.println("Robo2 venceu");
				} else {
					System.out.println("Empate");
				}
			} else if (ponto1 == 1) {
				if (numPar1 > numPar2) {
					System.out.println("Robo1 venceu");
				} else if (numPar2 > numPar1) {
					System.out.println("Robo2 venceu");
				} else {
					System.out.println("Empate");
				}
			} else {
				System.out.println("Empate");
			}
		}
	}
}

import java.util.Scanner;

public class Main {

	public static void Converter(int[] array) {
		int sum = 0;
		for (int i = 0; i < array.length; i++) {
			if (i == array.length - 1) {
				sum = sum + array[i];
			} else {
				if (array[i + 1] > array[i]) {
					sum = sum + (-array[i]);
				} else {
					sum = sum + array[i];
				}
			}
		}
		System.out.println(sum);
	}

	public static void main(String[] args) {
		Scanner entrada = new Scanner(System.in);
		System.out.println("Digite o numero em romano");
		String input = entrada.next().toUpperCase();
		char[] CharArray = new char[input.length()];
		for (int i = 0; i < CharArray.length; i++) {
			CharArray[i] = input.charAt(i);
		}
		int[] IntArray = new int[CharArray.length];
		for (int i = 0; i < CharArray.length; i++) {
			switch (CharArray[i]) {
			case 'I':
				IntArray[i] = 1;
				break;
			case 'V':
				IntArray[i] = 5;
				break;
			case 'X':
				IntArray[i] = 10;
				break;
			case 'L':
				IntArray[i] = 50;
				break;
			case 'C':
				IntArray[i] = 100;
				break;
			case 'D':
				IntArray[i] = 500;
				break;
			case 'M':
				IntArray[i] = 1000;
				break;
			}
		}
		System.out.println();
		Converter(IntArray);
	}
}
